#!/usr/bin/python
import picamera
import time

camera = picamera.PiCamera(resolution=(1944, 2592), framerate=2)
print "analog_gain = " + str(float(camera.analog_gain))
print "awb_mode = " + str(camera.awb_mode)
print "awb_gains = " + str(camera.awb_gains)
print "exposure_mode = " + str(camera.exposure_mode)
print "exposure_speed = " + str(camera.exposure_speed)
print "shutter_speed = " + str(camera.shutter_speed)

camera.iso = 100
camera.zoom = (0.0, 0.0, 1.0, 1.0)

max_wait = 1.0
total_wait = 0.0
while camera.analog_gain < 1.0 and camera.digital_gain < 1.3 and total_wait <= max_wait:
   time.sleep(0.5)
   total_wait = total_wait + 0.5
   print ("x analog gain = " + str(float(camera.analog_gain)))
   print ("x digital gain = " + str(float(camera.digital_gain)))
   print ("x iso = " + str(camera.iso))
   print "----- looping -------"
            
if total_wait >= max_wait :
   log.info("max timeout reached for adjusting sensors. analog gain = %s,  digital gain = %s", str(float(camera.analog_gain)),str(float(camera.digital_gain))

        
#print "----- sleeping for 3s -------"
#time.sleep(3)

camera.shutter_speed = 10000
camera.exposure_mode = 'off'
auto_gainlevel = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = auto_gainlevel

print "analog_gain = " + str(float(camera.analog_gain))
print "awb_mode = " + str(camera.awb_mode)
print "awb_gains = " + str(camera.awb_gains)
print "exposure_mode = " + str(camera.exposure_mode)
print "exposure_speed = " + str(camera.exposure_speed)
print "shutter_speed = " + str(camera.shutter_speed)

print "----- resetting sensor -------"

camera.shutter_speed = 0
#camera.exposure_speed = 0 -- can't be set
camera.exposure_mode = 'auto'
camera.awb_mode = 'auto'

print "----- sleeping for 3s -------"
time.sleep(3)

camera.shutter_speed = 10000
camera.exposure_mode = 'off'
auto_gainlevel = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = auto_gainlevel

print "analog_gain = " + str(float(camera.analog_gain))
print "awb_mode = " + str(camera.awb_mode)
print "awb_gains = " + str(camera.awb_gains)
print "exposure_mode = " + str(camera.exposure_mode)
print "exposure_speed = " + str(camera.exposure_speed)
print "shutter_speed = " + str(camera.shutter_speed)


camera.close() 
