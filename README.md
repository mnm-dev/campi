# Multicast based python picture cluster tools

This is a quick hack for a set of tools  to control a set of worker nodes to take pictures or stream videos
It is based on the concept from [http://www.pi3dscan.com/] (http://www.instructables.com/id/Multiple-Raspberry-PI-3D-Scanner/)

The set-up is a set of worker nodes (raspberry pi with camera modules) and a controller node.

## TODO

* use a different user on the worker nodes
* find master via multi-cast
* requests seems to slow down the service startup, look at urllib
* pi boot is slow (~30 seconds till service starts, more to get ip, 14 to start service, total 60 sec)
* pics are slow
* make less hacky
 
### commands

Each command is received via a multi-cast message and a response is sent to HTTP to the master node from each worker node

Each multi-cast messages is a command with optional colon separated arguments.
The message can be at most 400 bytes (arbitrarily set)

command {: argument }*

If the command has any arguments, the first argument has to be the `set of worker node IDs` being targeted.
If there are not arguments, all worker nodes are targeted.
 
The `set of worker node IDs` can be a `*` to target all nodes or a comma separated list of node ids.
The node id is he last IP4 byte minus some offset to allow the first worker node to have the id 1


In the below table `ids` refer to `set of worker node IDs` as described above and set-id refers to some string that can be used to identify a image set.
When e.g.a photo is taken, all images are stored in a folder with that `setid` as name
Parenthesis indicate the arguments are optional.


 command       | data      | response url         | usage
:--------------|:--------- |:---------------------|:-------------------------------------------------------------------------------------
ping           | (ids)     | /pong/{id}           | simple health check, /pong/{id} is also called by the workers when they first come up
reboot         | (ids)     | /reboot/{id}         | reboot the relevant nodes (e.g. when a camera plays up) 
shutdown       | (ids)     | /shutdown/{id}       | shutdown the relevant nodes 
refresh_script | (ids)     | /refresh/script/{id} | when a new version of the worker service or worker scripts  should be used
capture_thumb  | ids:setid | /capture/thumb/{id}   | calls `campibase/capturePic.sh thumb imageName`
capture_full   | ids:setid | /capture/full/{id}    | calls `campibase/capturePic.sh full imageName`
stream_on      | (ids)     | /stream/on/{id}       | calls `campibase/startStreaming.sh`
stream_off     | (ids)     | /stream/off/{id}      | calls `campibase/stopStreaming.sh`


## Setup

All nodes have access to the same **cifs share under /opt/campibase**, which is used to hold 

* the main worker scripts and configuration
* images stored by worker nodes
* logs for all nodes

This git repo is assumed to be the content of /opt/campibase.

Because camera positioning is important, the worker nodes are using DHCP from a router to set their IPs in a particular order and the last byte of the IP4 address is used to identify the worker. The load of managing this address allocation is on the relevant dhcp server of the use network .
To have some flexibility  with different subnets, the worker script uses a configurable off-set and the samba server ip and to simplify this test the samba server IP and the controller node resolved in the /etc/hosts file of the worker nodes.

All worker nodes should run the bare minimum (e.g. no x)
For an initial set-up of a worker node image - assuming that images are used for all other worker nodes:

### configuration

These settings are depending on how the network is setup and the samba share user/password

The campi-prepare-service.sh waits for an IP in the 192.168.0.0/16 or 10.0.0.0/8 subnets.

    # hard coding master names
    cat >> /etc/hosts <<ENDL
    192.168.121.71  campibase-server
    192.168.121.71  master-node
    ENDL

    # setting up the samba share on the worker nodes
    echo '//campibase-server/campibase  /opt/campibase  cifs   rw,_netdev,vers=3.0,user=pi,password=FakePassword  0 0' >> /etc/fstab


### example samba setup

On the samba server:

    mkdir /opt/campibase
    cd /opt/campibase
    git clone https://bitbucket.org/mnm-dev/campi.git .

    chmod a+rwX /opt/campibase/logs /opt/campibase/images

    # trying to match the uid of the pi worker node users, but this can be handled by many other ways
    useradd --uid 1001 -M -s /sbin/nologin pi


    chown pi /opt/campibase/images /opt/campibase/logs
    smbpasswd -a pi
    # FakePassword
    smbpasswd -e pi

    # /etc/samba/smb.conf
    [global]
        workgroup = pigroup
        map to guest = Bad User
        netbios name = bosspi
        security = user
        encrypt passwords = yes
        smb passwd file = /etc/samba/smbpasswd
    [database]
        valid users = pi
        invalid users =  root
    [campibase]
        inherit acls = Yes
        path = /opt/campibase
        read only = No


### bootstrap scripts

The bootstrap scripts are using the hosts and fstab configuration from the previous step.
They should not require changes after the first set-up and are only responsible for preparing for and starting the actual worker script of the samba share.

    # initial mount to get one-off bootstrap scripts (this is done by the service on boot)
    mkdir -p /opt/campibase
    mount /opt/campibase
    /bin/bash -x /opt/campibase/scripts/worker-node-setup/setup-worker-service.sh


### hard coded configuration

The `/opt/campibase` base dir is hard coded on the fstab mount and the preparation script.
The master-node name is used in hosts file and the campiworker.py worker script.
The relative paths under `/opt/campibase` are hard-coded everywhere, without the base dir name 
The script names for starting, restarting killing or stopping.

localImageShare=`/opt/campiLocalImage` is set in the preparation script and passed to the worker

