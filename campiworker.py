#!/usr/bin/python
'''

Simple python script to listen to multi cast commands and action the command and respond to a hardcoded controller via http

The expectation is that a controller raspberry pi node exposes a smb share that is mounted by the campi systemd service on the worker.
The script is called from that share, started by the campi systemd service

max multi cast data size is 400

raspistill is used for stills


Created 2018-05-26

@author: markus
'''

import errno
import logging
import os
import shutil
import socket
import struct
import subprocess
import sys
import threading
import time

import RPi.GPIO as GPIO
import picamera
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')

log = logging.getLogger(__name__)

# Pi B - Peripheral Base Address 0x20000000; ACT LED GPIO 16 (inverted 0=on)
# Pi B+ - Peripheral Base Address 0x20000000; ACT LED GPIO 47
# Pi 2B - Peripheral Base Address 0x3f000000; ACT LED GPIO 47

GPIO_ACT_LED_CHANNEL = 47

def prepare_led():
    """
    prepares the GPIO channel to control the OK/ACT LED and flashes it quickly
    """
    sleep_time = 0.05
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_ACT_LED_CHANNEL, GPIO.OUT)
    for _unused in range(4):
        led_light_on()
        time.sleep(sleep_time)
        led_light_off()
        time.sleep(sleep_time)

def led_light_on():
    """
    turn the green pi led on
    """
    GPIO.output(GPIO_ACT_LED_CHANNEL, GPIO.HIGH)


def led_light_off():
    """
    turn the green pi led off
    """
    GPIO.output(GPIO_ACT_LED_CHANNEL, GPIO.LOW)


def mkdirs_for_file(filename):
    file_dir = os.path.dirname(filename)
    try:
        os.makedirs(file_dir)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(file_dir):
            pass
        else:
            raise


def ack_response(cmd, attempts=3):
    '''
    general response / requests-wrapper
    '''
    url = "{baseurl}/{api}/{workerid}".format(baseurl=cmd.env.controller_url, api=cmd.callback, workerid=cmd.env.worker_id)
    resp = rrequest(retries=attempts).get(url)
    if resp.status_code != 200:
        log.error("Could not call %s, after %s attempts", url, attempts)
    else:
        log.debug("called %s", url)

def pong(cmd, attempts=9):
    '''
    simple health check
    '''
    ack_response(cmd, attempts)

def blink(cmd):
    '''
    make led turn on for 3 sec unless specified differently
    '''

    sleep_time = 3
    repeat = 1
    args = []

    if len(cmd.call_args) > 1 :
       args = cmd.call_args[1].split("-")

    if len(args) > 0 :
       stime = args.pop(0)
       if stime != '':
          sleep_time = int(stime)
    
    if len(args) > 0 :
       srepeat = args.pop(0)
       if srepeat != '':
          repeat = int(srepeat)
       
    log.debug("blink params sleep=%s, repeat=%s", sleep_time, repeat)

    for _unused in range(repeat):
       led_light_on()
       time.sleep(sleep_time)
       led_light_off()
       time.sleep(0.5)

def reboot(cmd):
    '''
    ack and reboot
    '''
    ack_response(cmd)
    subprocess.call('sudo reboot', shell=True)


def refresh_script(cmd):
    '''
    run the setup-worker-script and restart the campi (worker) service
    '''
    log.info("reloading service scripts and restarting campi worker")
    subprocess.call("/bin/bash {path}/scripts/worker-node-setup/setup-worker-service.sh".format(path=cmd.env.base_path), shell=True)
    ack_response(cmd)
    subprocess.call('sudo systemctl restart campistart.service', shell=True)
    sys.exit(0)

def capture_full(cmd):
    '''
    capture_full calls capturePic with a setting of high
    '''
    local_file = "{local_path}/{setid}_{wrkid:0>3}-{size}.jpg".format(
        setid=cmd.call_args[1], wrkid=cmd.env.worker_id,
        local_path=cmd.env.local_img_base, size="full")
    remote_file = "{path}/{setid}/{wrkid:0>3}-{size}.jpg".format(
        path=cmd.env.remote_img_path, setid=cmd.call_args[1], wrkid=cmd.env.worker_id, size="full")
    log.info("capturing still - mkdir: %s", local_file)
    mkdirs_for_file(local_file)
    log.info("capturing still - camera capture: %s, settings: iso=%s, shutter=%s", local_file, str(cmd.env.camera.iso), str(cmd.env.camera.shutter_speed))
    cmd.env.camera.capture(local_file, thumbnail=None)
    log.info("capturing still completed: %s", local_file)
    ack_response(cmd)
    BackgroundImageMove(local_file, remote_file)


def shutdown(cmd):
    '''
    ack command and shutdown
    '''
    ack_response(cmd)
    subprocess.call('sudo shutdown now -h', shell=True)


def stream_on(cmd):
    '''
    call the startStreaming script
    '''
    subprocess.call("{0}/scripts/worker-nodes/startStreaming.sh".format(cmd.env.base_path), shell=True)
    ack_response(cmd)


def stream_off(cmd):
    '''
    call the stopStreaming script
    '''
    subprocess.call("{0}/scripts/worker-nodes/stopStreaming.sh".format(cmd.env.base_path), shell=True)
    ack_response(cmd)


def setup_mcast(mcast_grp, mcast_port):
    '''
    setup the multicast socket
    '''
    log.info("starting multi cast socket on grp %s with port %s", mcast_grp, mcast_port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', mcast_port))
    mreq = struct.pack("4sl", socket.inet_aton(mcast_grp), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    return sock


def wait_for_datagram(sock):
    '''
    wait for package on the on the passed socket
    '''
    try:
        return sock.recv(400)
    except KeyboardInterrupt:
        log.info("bailing out after keyboard interrupt")
        sys.exit(0)


def rrequest(retries=5, backoff_factor=0.3, status_forcelist=(500, 403, 502, 504), session=None):
    '''
    create a Retry session for all requests
    '''
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def split_datagram(datagram):
    """
    split the datagram into a command name and command argument array using colons
    """
    cmd_args = datagram.split(":")
    cmd_name = cmd_args.pop(0).lower()
    return cmd_name, cmd_args


class BackgroundImageMove(object):
    """
    This is responsible for moving an image file from local storage to the server
    The move happens in a two stage process with the first part mobing the file
    with an extension of .move which is removed once the file has been moved
    Optionally we could implement a second acknowledgement to the server
    """

    def __init__(self, file_source, file_target):
        """ Constructor
        :file_source the path of the source file
        :file_target interval: Check interval, in seconds
        """
        self.file_source = file_source
        self.file_target = file_target

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        """
         get the target folder
         mkdirs for the target folder
         move the file with .move
         remove (rename/move) the target .move extension
         signal ack
        """
        log.debug("moving %s", self.file_target)
        mkdirs_for_file(self.file_target)
        shutil.move(self.file_source, self.file_target + ".move")
        os.rename(self.file_target + ".move", self.file_target)
        log.info("moved %s", self.file_target)


class Environment(object):
    '''
    Environment is the environment the command runs in
    It is initialized on worker start up with default configuration
    '''
    def __init__(self, controller_url, worker_id, base_path, local_img_base):
        self.controller_url = controller_url
        self.worker_id = worker_id
        self.base_path = base_path
        self.local_img_base = local_img_base
        self.remote_img_path = "{0}/{1}".format(base_path, "images")
        
        camera = picamera.PiCamera(resolution=(2592, 1944), framerate=2)
        camera.iso = 100
        camera.zoom = (0.0, 0.0, 1.0, 1.0)
        
        # let the camera adjust to light levels (automatic gain control should settle)
        max_wait = 10.0
        total_wait = 0.0
        while camera.analog_gain < 1.0 and camera.digital_gain < 1.3 and total_wait <= max_wait:
            time.sleep(0.5)
            total_wait = total_wait + 0.5
            log.info("analog gain = %s,  digital gain = %s, iso = %s", str(float(camera.analog_gain)), str(float(camera.digital_gain)), str(camera.iso))
            
        if total_wait >= max_wait :
            log.info("max timeout reached for adjusting sensors. analog gain = %s,  digital gain = %s, iso = %s", str(float(camera.analog_gain)), str(float(camera.digital_gain)), str(camera.iso))
            
        # fix values
        log.info("fixing sensor values")
        camera.shutter_speed = 10000
        camera.exposure_mode = 'off'
        auto_gainlevel = camera.awb_gains
        camera.awb_mode = 'off'
        camera.awb_gains = auto_gainlevel
        self.camera = camera


class Command(object):
    """
    wrapper
    """
    def __init__(self, environment, command_description, call_args):
        self.env = environment
        self.call = command_description.call
        self.callback = command_description.callback
        self.call_args = call_args


    def execute(self):
        '''
        execute this commands when this worker was targeted
        '''
        if self.is_matching_worker_id():
            log.info("worker %s executes %s", self.env.worker_id, self.call.__name__)
            led_light_on()
            self.call(self)
            led_light_off()


    def is_matching_worker_id(self):
        '''
        Check if the received command was meant for this worker
        '''
        worker_id = self.env.worker_id
        worker_ids = None
        try:
            worker_ids = self.call_args[0]
            if worker_ids != "*" and not worker_id in worker_ids.split(","):
                log.debug("worker '%s' is not matching targeted ids: '%s'", worker_id, worker_ids)
                return False
        except IndexError:
            # no arguments means this worker was selected
            pass

        log.debug("worker '%s' was selected:'%s'", worker_id, worker_ids)
        return True


class CommandDefinitions(object):
    '''
    describes a single call and callback-url-context pair
    '''
    def __init__(self, call, callback):
        self.call = call
        self.callback = callback


command_definitions = {
    "ping" : CommandDefinitions(pong, "pong"),
    "refresh_script" : CommandDefinitions(refresh_script, "refresh/script"),
    "reboot": CommandDefinitions(reboot, "reboot"),
    "shutdown" : CommandDefinitions(shutdown, "shutdown"),
    "capture_full" : CommandDefinitions(capture_full, "capture/full"),
    "stream_on" : CommandDefinitions(stream_on, "stream/on"),
    "stream_off" : CommandDefinitions(stream_off, "stream/off"),
    "blink" : CommandDefinitions(blink, "blink"),
}


def main():
    """ the worker main"""

    mcast_grp = '224.1.1.1'
    mcast_port = 7337

    campi_controller = sys.argv[1]
    my_ip = sys.argv[2]
    base_path = sys.argv[3]
    local_img_base = sys.argv[4]

    # the worker id is the fourth my_ip byte minus some constant offset
    worker_id = str(int(my_ip.split(".")[3]) - 100)
    controller_url = "http://{0}:8888/slave/api/response".format(campi_controller)
    log.info("Starting campi worker %s (%s) against controller %s listening on %s:%s"
             , worker_id, my_ip, controller_url, mcast_grp, mcast_port)

    environment = Environment(controller_url, worker_id, base_path, local_img_base)
    log.debug("environment: %s", vars(environment))

    sock = setup_mcast(mcast_grp, mcast_port)

    # wait for the controller to be up with a single ping and a long retry period
    cmd = Command(environment, command_definitions.get("ping"), [])
    pong(cmd, 12)
    prepare_led()


    while True:
        try:
            datagram = wait_for_datagram(sock)
            (cmd_name, cmd_args) = split_datagram(datagram)
            cmd_desc = command_definitions.get(cmd_name)
            if not cmd_desc:
                log.warn("Ignoring undefined command '%s'", cmd_name)
            else:
                cmd = Command(environment, cmd_desc, cmd_args)
                cmd.execute()

        except Exception:
            log.exception("ignoring exception")


if __name__ == "__main__":
    main()
