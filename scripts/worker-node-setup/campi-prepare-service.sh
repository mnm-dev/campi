#!/bin/bash -evx

fileShareMaster=campibase-server
commandMaster=master-node
campibase=/opt/campibase
logDir=${campibase}/logs
localImageShare=/opt/campiLocalImage

getIp()
{
ifconfig | grep "inet " | grep -e " 192\.168" -e " 10\." | awk '{print $2}' | sort | head -n 1
}

IP=$(getIp)
while [[ -z ${IP} ]];do
    echo "no ip"
    ifconfig
    echo sleeping
    sleep 1
    IP=$(getIp)
done

logFile=${logDir}/${IP}.$( date "+%Y%m%d" ).log

echo logging to ${logFile} 

while ! ping -c1 -W1 ${fileShareMaster} &> /dev/null; do
  echo waiting for ${fileShareMaster}
  sleep 1
done

mkdir -p ${localImageShare}
# storing to a ramdisk ~(17mb/s) is usually 50-100ms faster than the sd card (~14mb/s)
# but both are several hundred ms faster (~1s) than saving directly to a share
if ! grep ${localImageShare} /proc/mounts &>/dev/null; then
   mount -t tmpfs -o size=30m tmpfs ${localImageShare}
fi

while ! grep ${fileShareMaster} /proc/mounts &>/dev/null; do
    echo attemting to mounting ${campibase}
    mount ${campibase} && true # ignore errors
    sleep 5
done
echo ${campibase} is mounted


# take ownership of of the OK / ACT led, so that other activity doesn't use it
echo none  >/sys/class/leds/led0/trigger


# kill any other script
echo "terminating any other running campiworker with 'pkill -9 -f python ${campibase}/campiworker.py'"
pkill --echo -9 -f "python ${campibase}/campiworker.py" && true

echo "$( date "+%Y%m%d_%H%M%S" ) starting campiworker.py" | tee --append ${logFile} 
nohup ${campibase}/campiworker.py ${commandMaster} ${IP} ${campibase} ${localImageShare} &>> ${logFile} &
