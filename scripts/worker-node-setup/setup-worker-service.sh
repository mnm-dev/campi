#!/bin/bash


cp -auf /opt/campibase/scripts/worker-node-setup/campistart.service /etc/systemd/system
sudo systemctl enable campistart.service
cp -auf /opt/campibase/scripts/worker-node-setup/campi-prepare-service.sh /opt/campi-prepare-service.sh
chmod u+x /opt/campi-prepare-service.sh

# don't start the service in the setup