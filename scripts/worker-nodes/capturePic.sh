#!/bin/bash

extension=".jpg"

sizeMode="$1"
localImageBase="$2"
remoteImagePath="$3"
remoteImageDir="$(dirname "${remoteImagePath}")"
imageName="$(basename "${remoteImagePath}")"
imageNameNoExtension="${imageName/${extension}/}"
localImage="${localImageBase}/${imageNameNoExtension}"


echo "$( date "+%Y%m%d %H%M%S.%N" ) - start local raspistill ${sizeMode} ${imageName}"
if [[ ${sizeMode} == "full" ]]; then
    time raspistill --verbose --nopreview --thumb none \
         -t 600 -ISO 100 -ss 10000 -w 1944 -h 2592 -roi 0,0,1,1 -o "${localImage}"
else 
    time raspistill --verbose --nopreview --thumb none \
         -t 1 -ISO 100 -ss 10000 -w 800 -h 600 -q 50 -o "${localImage}"
fi
echo "$( date "+%Y%m%d %H%M%S.%N" ) - completed local raspistill ${imageName}"

( echo "$( date "+%Y%m%d %H%M%S.%N" ) - moving ${localImage}" to "${remoteImagePath}" &&
    mkdir -p ${remoteImageDir} &&
    ls -al "${localImageBase}" &&
    time mv -f "${localImage}" "${remoteImageDir}/${imageNameNoExtension}" &&
    mv -f "${remoteImageDir}/${imageNameNoExtension}" "${remoteImagePath}" &&
    ls -al "${remoteImagePath}" &&
    echo "$( date "+%Y%m%d %H%M%S.%N" ) - moved local ${imageName} to ${remoteImagePath}" ) &
